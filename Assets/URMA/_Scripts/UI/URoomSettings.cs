﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(bl_UIReferences))]
public class URoomSettings : MonoBehaviour {

    public GameObject EnemiesAmountUI;
    public InputField enemiesAmount;
    public Text startGameButtonText;
    bl_UIReferences uiRef;
    bl_GameData gameData;

    private void Start()
    {
        uiRef = GetComponent<bl_UIReferences>();
        gameData = bl_GameData.Instance;
        enemiesAmount.text = 10.ToString();

        EnemiesAmountUI.gameObject.SetActive(PhotonNetwork.isMasterClient);
    }

    bool questStarted = false;

    public void ButtonClick_CommanderGameLaunched () {
        if (!PhotonNetwork.isMasterClient) {
            return;
        }

        if (questStarted) {
            return;
        } else {
            questStarted = true;
            startGameButtonText.text = "Режим польоту";
        }

        try {
            ur_MobStartPoint spawner = FindObjectOfType<ur_MobStartPoint>();
            spawner.SpawnMobs(int.Parse(enemiesAmount.text));
        } catch (Exception e) {
            Debug.LogError(e.ToString());
        }

        PhotonNetwork.room.IsVisible = false;
        EnemiesAmountUI.gameObject.SetActive(false);

        if (!gameData.DebugVersion)
        {
            PhotonView p = GetComponent<PhotonView>();
            p.RPC("RPC_OnGameLaunchedByCommander", PhotonTargets.OthersBuffered); 
        }
    }

    [PunRPC]
    public void RPC_OnGameLaunchedByCommander () {
        uiRef.JoinTeam(0);
    }
}
