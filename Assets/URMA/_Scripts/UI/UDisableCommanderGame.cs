﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UDisableCommanderGame : MonoBehaviour {

    void Start()
    {
        var g = bl_GameData.Instance;
        if (!g.DebugVersion)
        {
            gameObject.SetActive(false);
        } 
    }
}
