﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class UTankInteractionUI : MonoBehaviour {

    public GameObject enterTankUIdriver;
    public GameObject enterTankUI;
    public GameObject leaveTankUI;

    internal void OnTankStateChanged(ur_Tank tank, bool inTank)
    {
        bool hasTank = tank;

        if (!hasTank) {
            enterTankUIdriver.SetActive(false);
            enterTankUI.SetActive(false);
            leaveTankUI.SetActive(false);
        } else {
            bool canBeDriver = tank.PersonsInside == 0;
            if (inTank) {
                enterTankUIdriver.SetActive(false);
                enterTankUI.SetActive(false);
            } else {
                enterTankUIdriver.SetActive(canBeDriver);
                enterTankUI.SetActive(!canBeDriver);
            }

            leaveTankUI.SetActive(inTank);
        }
    }
}
