﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UDisableCheckCommander : MonoBehaviour {

    public bool disableIfCommander;

	void Start () {
        var g = bl_GameData.Instance;
        if (g.DebugVersion) {
            return;
        }

        if (g.isCommander && disableIfCommander) {
            gameObject.SetActive(false);
        } else
            if (!g.isCommander && !disableIfCommander)
        {
            gameObject.SetActive(false);
        }
	}
}
