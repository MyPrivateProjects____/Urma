﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UPlayerEnemyCaptivator : MonoBehaviour {

    public GameObject captivateEnemyUI;

    internal void OnCaptivatorStateChanged(bool hasCaptiveCandidate)
    {
        captivateEnemyUI.gameObject.SetActive(hasCaptiveCandidate);
    }
}
