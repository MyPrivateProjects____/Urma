﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartSceneManager : MonoBehaviour {
    
    public bool IsSecurityPass { get { return enteringGame; } }

	void Awake () {
        LaunchGameIfDeviceIDEntered();
	}

	public void TrySecurityPass(string password)
	{
        var gameData = bl_GameData.Instance;
        if (password == gameData.appPassword) {
            SaveDeviceID();
        }
        LaunchGameIfDeviceIDEntered();
	}

    string DEVICE_ID_KEY = "DEVICE_ID_KEY";

    void SaveDeviceID () {
        PlayerPrefs.SetString(DEVICE_ID_KEY, SystemInfo.deviceUniqueIdentifier);
    }

    bool enteringGame = false;

    void LaunchGameIfDeviceIDEntered()
    {
        string actualDeviceID = SystemInfo.deviceUniqueIdentifier;
        string savedDeviceID = PlayerPrefs.GetString("DEVICE_ID_KEY");

        if (actualDeviceID == savedDeviceID) {
            enteringGame = true;
            UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
        }
    }
}
