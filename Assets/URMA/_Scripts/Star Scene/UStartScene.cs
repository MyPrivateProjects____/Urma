﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UStartScene : MonoBehaviour {

    public StartSceneManager startSceneManager;
    public InputField inputFieldPassword;
    public Button buttonEnterPassword;
	
	void Start () {
        if (startSceneManager.IsSecurityPass) {
            gameObject.SetActive(false);
        } else {
            InitUI();
        }
	}

    private void InitUI()
    {
        buttonEnterPassword.onClick.AddListener(ButtonClick_EnterPassword);
    }

    private void ButtonClick_EnterPassword()
    {
        if (startSceneManager.IsSecurityPass)
        {
            return;
        }

        string password = inputFieldPassword.text;
        startSceneManager.TrySecurityPass(password);
    }
}
