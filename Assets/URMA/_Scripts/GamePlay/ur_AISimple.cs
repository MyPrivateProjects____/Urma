﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ur_AISimple : bl_MonoBehaviour {

    bl_PlayerDamageManager _me;
    bl_PlayerAnimations _animator;

    public Transform Target;
    [Space(5)]
    [Header("AI")]
    public float PatrolRadius = 20f; //Radius for get the random point
    public float LookRange = 25.0f;   //when the AI starts to look at the player
    public float FollowRange = 10.0f;       //when the AI starts to chase the player
    public float AttackRange = 2;         // when the AI stars to attack the player
    public float CritAttackRange = 10f;
    public float LosseRange = 50f;
    public float RotationLerp = 6.0f;

    [Space(5)]
    [Header("Attack")]
    public float AttackRate = 3;
    public float Damage = 20; // The damage AI give
    [Space(5)]
    [Header("AutoTargets")]
    public List<Transform> PlayersInRoom = new List<Transform>();//All Players in room
    public float UpdatePlayerEach = 5f;
    public bl_GunManager gunManager;

    //Privates
    private Vector3 correctPlayerPos = Vector3.zero; // We lerp towards this
    private Quaternion correctPlayerRot = Quaternion.identity; // We lerp towards this
    private float attackTime;
    private UnityEngine.AI.NavMeshAgent Agent = null;
    private bool personal = false;

    public bool IsCaptive { get; private set; }
    Transform _captivator;

    protected override void Awake()
    {
        base.Awake();
        _me = this.GetComponent<bl_PlayerDamageManager>();
        _animator = this.GetComponentInChildren<bl_PlayerAnimations>();
        Agent = this.GetComponent<UnityEngine.AI.NavMeshAgent>();
        InvokeRepeating("UpdateList", 1, UpdatePlayerEach);
    }

	internal void Captivate(PhotonView captivator)
	{
        if (IsCaptive) {
            return;
        }
        photonView.RPC("RPC_SetCaptivated", PhotonTargets.All, captivator.viewID);
	}

    [PunRPC]
    public void RPC_SetCaptivated (int captivatorViewID) {
        PhotonView captivator = PhotonView.Find(captivatorViewID);

        IsCaptive = true;
        _captivator = captivator.transform;
        _animator.SetCaptive(true);

        ur_AISync a = GetComponent<ur_AISync>();
        a.DropGun();
    }

    void FollowMyCaptivator()
    {
        if (PhotonNetwork.isMasterClient)
        {
            if (_captivator)
            {
                Agent.destination = _captivator.transform.position;
            }
        }
    }

	void Start()
    {
        attackTime = Time.time;
    }

    void CheckAttackTarget () {
        if (PhotonNetwork.isMasterClient)//All AI logic only master client can make it functional
        {
            if (Target == null)
            {
                //Get the player most near
                for (int i = 0; i < PlayersInRoom.Count; i++)
                {
                    if (PlayersInRoom[i] != null)
                    {
                        float Distance = Vector3.Distance(PlayersInRoom[i].position, transform.position);//if a player in range, get this

                        if (Distance < LookRange)//if in range
                        {
                            GetTarget(PlayersInRoom[i]);//get this player
                        }
                    }
                }
                //if target null yet, the patrol
                if (!Agent.hasPath)
                {
                    RandomPatrol();
                }
            }
        }
    }

    void ChaseTarget () {
        float Distance = Vector3.Distance(Target.position, transform.position);
        if (Distance < LookRange)//if in range
        {
            Look();
        }

        if (Distance > LookRange)//if the target not in the range, then patrol
        {
            //                GetComponent<Renderer>().material.color = Color.green;     // if you want the AI to be green when it not can see you.
            if (PhotonNetwork.isMasterClient)
            {
                if (!Agent.hasPath)
                {
                    RandomPatrol();
                }
            }
        }

        if (PhotonNetwork.isMasterClient)
        {
            if (Distance < AttackRange)
            {
                Attack();
            }
        }

        if (Distance < FollowRange || personal)
        {
            Follow();
        }

        if (PhotonNetwork.isMasterClient)
        {
            if (Distance >= LosseRange && !personal)
            {
                photonView.RPC("ResetTarget", PhotonTargets.AllBuffered);
                if (!Agent.hasPath)
                {
                    RandomPatrol();
                }
            }
        }
    }

    public bool IsDead {
        get {
            return _me.dead;
        }
    }

    public override void OnUpdate()
    {
        if (IsDead) {
            this.Agent.enabled = false;
            return;
        }

        if (IsCaptive) {
            FollowMyCaptivator();
        } else {
            CheckAttackTarget();

            if (Target != null)
            {
                ChaseTarget();
            }
        }
    }

    /// <summary>
    /// If player not in range then the AI patrol in map
    /// </summary>
    void RandomPatrol()
    {
        Vector3 randomDirection = Random.insideUnitSphere * PatrolRadius;
        randomDirection += transform.position;
        UnityEngine.AI.NavMeshHit hit;
        UnityEngine.AI.NavMesh.SamplePosition(randomDirection, out hit, PatrolRadius, 1);
        Vector3 finalPosition = hit.position;
        Agent.SetDestination(finalPosition);
    }
    /// <summary>
    /// 
    /// </summary>
    void Look()
    {
//        GetComponent<Renderer>().material.color = Color.yellow;
        if (PhotonNetwork.isMasterClient)
        {
            Quaternion rotation = Quaternion.LookRotation(Target.position - transform.position);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * RotationLerp);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    void Follow()
    {
        if (PhotonNetwork.isMasterClient)
        {
            Agent.destination = Target.transform.position;
        }
//        GetComponent<Renderer>().material.color = Color.red;
    }

    public bl_NetworkGun currentNetworkGun;

    /// <summary>
    /// 
    /// </summary>
    void Attack()
    {
        if (Time.time > attackTime)
        {
            bl_PlayerDamageManager pdm = Target.root.GetComponent<bl_PlayerDamageManager>();
            if (pdm != null)
            {
                bl_OnDamageInfo di = new bl_OnDamageInfo();
                di.mActor = null;
                di.mDamage = float.MaxValue;
                di.mDirection = this.transform.position;
                di.mFrom = "AI";

                bool shouldDamage = true;
                float distance = (Target.transform.position - transform.position).magnitude;

                float chanceAttack = 1f;
                if (AttackRange > CritAttackRange) {
                    chanceAttack = 1f - ((distance - CritAttackRange) / (AttackRange - CritAttackRange));
                }

                chanceAttack = Mathf.Clamp(chanceAttack, 0f, 1f);
                chanceAttack = chanceAttack * chanceAttack * chanceAttack;

                shouldDamage = chanceAttack > UnityEngine.Random.Range(0f, 1f);

                Debug.Log( "Distance = " + distance + "; attack chance = " + chanceAttack);
                if (shouldDamage) {
                    pdm.GetDamage(di);
                    Debug.Log("Send Damage!");
                } else {
                    Debug.Log("Miss!");
                }

                attackTime = Time.time + AttackRate;

                float spread = gunManager.CurrentGun.spread;
                Vector3 firePos = Target.position;
                Quaternion fireRot = transform.rotation;

                photonView.RPC("RPC_FireSync", PhotonTargets.All, spread, firePos, fireRot);
            }
            else
            {
                Debug.Log("Can't found pdm in: " + Target.gameObject.name);
            }
        }
    }

    [PunRPC]
    void RPC_FireSync(float spread, Vector3 pos, Quaternion rot) {
        currentNetworkGun.Fire(spread, pos, rot);
    }
    /// <summary>
    /// 
    /// </summary>
    void GetTarget(Transform t)
    {
        Target = t;
        PhotonView view = GetPhotonView(Target.gameObject);
        if (view != null)
        {
            photonView.RPC("SyncTargetAI", PhotonTargets.OthersBuffered, view.viewID);
        }
        else
        {
            Debug.Log("This Target " + Target.name + "no have photonview");
        }
    }

    [PunRPC]
    void SyncTargetAI(int view)
    {
        GameObject g = FindPlayerRoot(view);
        if (g != null)
        {
            Transform t = g.transform;
            if (t != null)
            {
                Target = t;
            }
        }
    }

    [PunRPC]
    void ResetTarget()
    {
        Target = null;
    }

    void UpdateList()
    {
        PlayersInRoom = AllPlayers;
    }

    protected List<Transform> AllPlayers
    {
        get
        {
            List<Transform> list = new List<Transform>();
            foreach (PhotonPlayer p in PhotonNetwork.playerList)
            {
                GameObject g = FindPhotonPlayer(p);
                if (g != null)
                {
                    list.Add(g.transform);
                }
            }
            return list;
        }
    }
}
