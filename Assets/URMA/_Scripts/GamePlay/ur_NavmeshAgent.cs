﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class ur_NavmeshAgent : MonoBehaviour {
    public bool isGrounded { get { return true; } }
    public Vector3 Velocity { get { return _agent.velocity; } }

    NavMeshAgent _agent;

    void Awake () {
        _agent = GetComponent<NavMeshAgent>();
	}

    public int State
    {
        get
        {
            PlayerState res = PlayerState.Idle;

            if (Velocity.magnitude > 1f)
            {
                res = PlayerState.Running;
            }
            else
                if (Velocity.magnitude > 0.1f)
            {
                res = PlayerState.Walking;
            }

            return (int)res;
        }
    }
}
