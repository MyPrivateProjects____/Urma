﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ur_PlayerEnemyCaptivator : bl_MonoBehaviour {

    const string ENEMY_TAG = "AI";

    UnityCaptiveCandidateEvent _captiveCandidateEvent = new UnityCaptiveCandidateEvent();
    ur_AISimple _captivateCandidate;

	private void Start()
	{
        if (isMine)
        {
            UPlayerEnemyCaptivator ui;
            ui = FindObjectOfType<UPlayerEnemyCaptivator>();
            _captiveCandidateEvent.AddListener(ui.OnCaptivatorStateChanged);
        } else {
            enabled = false;
        }
	}

	void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(ENEMY_TAG))
        {
            var t = other.GetComponent<ur_AISimple>();
            if (!t.IsCaptive)
            {
                _captivateCandidate = t;
                OnCaptivatorStateChanged();
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(ENEMY_TAG))
        {
            if (_captivateCandidate && _captivateCandidate.gameObject == other.gameObject)
            {
                ClearCaptivateTarget();
            }
        }
    }

    void OnCaptivatorStateChanged () {
        _captiveCandidateEvent.Invoke(_captivateCandidate);
    }

	private void FixedUpdate()
	{
        if (_captivateCandidate &&  (_captivateCandidate.IsCaptive || _captivateCandidate.IsDead)) {
            ClearCaptivateTarget();
        }
	}

	void Update()
    {
        if (Input.GetKeyUp(KeyCode.R))
        {
            if (_captivateCandidate)
            {
                _captivateCandidate.Captivate(this.photonView);
                ClearCaptivateTarget();
            }
        }
    }

    void ClearCaptivateTarget () {
        _captivateCandidate = null;
        OnCaptivatorStateChanged();
    }

    [System.Serializable]
    public class UnityCaptiveCandidateEvent : UnityEvent<bool>
    {
    }
}
