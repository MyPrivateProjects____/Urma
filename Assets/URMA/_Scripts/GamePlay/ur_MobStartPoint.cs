﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ur_MobStartPoint : UnitySingleton<ur_MobStartPoint> {

    public float radius = 20f;

    public void SpawnMobs (int amount) {
        var GM = FindObjectOfType<bl_GameManager>();
        for (int i = 0; i < amount; i++) {
            SpawnMob();
        }
    }

    void SpawnMob () {
        float x = UnityEngine.Random.Range(-radius, radius);
        float z = UnityEngine.Random.Range(-radius, radius);
        Vector3 posOffset = new Vector3(x, 0f, z);
        Vector3 targetPos = transform.position + posOffset;
        SpawnMob(targetPos);
    }

    public void SpawnMob(Vector3 position)
    {
        PhotonNetwork.Instantiate(bl_GameData.Instance.Mob1.name, position, new Quaternion(), 0);
    }
}
