﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ur_PlayerTankInteractor : bl_MonoBehaviour {

    public CharacterController characterController;
    [HideInInspector]
    public UnityTankEvent playerTankInteractionEvent = new UnityTankEvent();
    public bl_FirstPersonController fpsController;
    public GameObject remotePlayerGO;
    public GameObject heatGO;

    private ur_Tank tankTarget;
    private bool _inTank;

	private void Start()
	{
        if (isMine) {
            UTankInteractionUI ui;
            ui = FindObjectOfType<UTankInteractionUI>();
            playerTankInteractionEvent.AddListener(ui.OnTankStateChanged);
        }
	}

	void Update () {
        if (Input.GetKeyUp(KeyCode.Return))
        {
            if (tankTarget)
            {
                InteractTank();
            }
        }
	}

    void InteractTank() {
        if (!_inTank) {
            bool sit = tankTarget.EnterTank();
            if (sit) {
                SitInTank(true);
            }
        } else {
            tankTarget.LeaveTank();
            SitInTank(false);
            UpdateState();
        }
    }

    public void SitInTank(bool sit)
    {
        photonView.RPC("RPC_SitInTank", PhotonTargets.AllBuffered, sit);
    }

    [PunRPC]
    void RPC_SitInTank(bool sitInTank)
    {
        _inTank = sitInTank;
        UpdateState();
    }

    void UpdateState () {
        if (isMine) {
            fpsController.enabled = !_inTank;
            heatGO.gameObject.SetActive(!_inTank);
            characterController.enabled = (!_inTank);
        } else {
            remotePlayerGO.gameObject.SetActive(!_inTank);
        }

        if (_inTank) {
            transform.SetParent(tankTarget.TankTransform);
        } else {
            transform.SetParent(null);
        }

        OnPlayerTankInteraction();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(ur_Tank.TANK_TAG)) {
            var t = other.GetComponent<ur_Tank>();
            if (t.CanEnter) {
                tankTarget = t;
                OnPlayerTankInteraction();
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(ur_Tank.TANK_TAG))
        {
            if (tankTarget && tankTarget.gameObject == other.gameObject) {
                tankTarget = null;
                OnPlayerTankInteraction();
            }
        }
    }

	private void FixedUpdate()
	{
        if (tankTarget) {
            OnPlayerTankInteraction();//update ui for persons inside tank
        }
	}

	void OnPlayerTankInteraction () {
        playerTankInteractionEvent.Invoke(tankTarget, _inTank);
    }

    [System.Serializable]
    public class UnityTankEvent : UnityEvent<ur_Tank, bool>
    {
    }

    internal void TryLeaveTank()
    {
        if (_inTank) {
            tankTarget.LeaveTank();
        }
    }
}
