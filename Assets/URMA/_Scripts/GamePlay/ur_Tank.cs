﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ur_Tank : MonoBehaviour {

    public static string TANK_TAG = "Tank";
    bool _isUsed = false;

    public bool CanEnter {
        get {
            return _personsInside >= 0 && _personsInside < 8;
        }
    }

    protected bool IsUsed { 
        get 
        { 
            return _isUsed;
        } 
        private set { 
            _isUsed = value;

            tankCamera.gameObject.SetActive(_isUsed);
            tankCamera.ChangeCamera(RTC_MainCamera.CameraMode.FPS);
            tankControll.enabled = _isUsed && photonView.isMine;
            if (tankGunController) {
                tankGunController.enabled = _isUsed;
            }
        }
    }

    public Transform TankTransform { get { return tankControll.transform; }}

    public RTC_MainCamera tankCamera;
    public RTC_TankController tankControll;
    public RTC_TankGunController tankGunController;
    PhotonView photonView;

    int _personsInside = 0;

    [PunRPC]
    public void RPC_EnterTank () {
        _personsInside++;
    }

    [PunRPC]
    public void RPC_LeaveTank()
    {
        _personsInside--;
    }

    public int PersonsInside {
        get {
            return _personsInside;
        }
    }

	private void Start()
	{
        photonView = GetComponent<PhotonView>();
        IsUsed = false;
	}

	public bool EnterTank () {
        if (IsUsed) {
            return false;
        }

        IsUsed = true;

        if (_personsInside == 0) {
            photonView.TransferOwnership(PhotonNetwork.player);
        }

        photonView.RPC("RPC_EnterTank", PhotonTargets.AllBuffered);

        return IsUsed;
    }

    public void LeaveTank()
    {
        IsUsed = false;

        photonView.RPC("RPC_LeaveTank", PhotonTargets.AllBuffered);
    }

	private void Update()
	{
        if (IsUsed) {
            if (tankCamera.cameraMode != RTC_MainCamera.CameraMode.FPS) {
                tankCamera.ChangeCamera(RTC_MainCamera.CameraMode.FPS);
            }
        }
	}
}
