﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleByDevice : MonoBehaviour {

	public Vector3 normalScale = Vector3.one;
	public Vector3 iphoneXScale = Vector3.one;

	void Start () {
		if (DeviceHelper.IsIPhoneX) {
			transform.localScale = iphoneXScale;
		} else {
			transform.localScale = normalScale;
		}
	}
}
