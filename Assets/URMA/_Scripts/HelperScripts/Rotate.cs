﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour {

    public float loaderRotationSpeed = -100f;
	
	void Update () {
        transform.Rotate(new Vector3(0f, 0f, Time.unscaledDeltaTime * loaderRotationSpeed));
	}
}
