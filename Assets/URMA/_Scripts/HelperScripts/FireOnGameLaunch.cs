﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FireOnGameLaunch : MonoBehaviour {

    public UnityEvent invokable;

	void Start () {
//        WorldManager.GetInstance.FireOnGameLaunched(OnGameLaunched);
	}

    private void OnGameLaunched()
    {
        invokable.Invoke();
    }
}
