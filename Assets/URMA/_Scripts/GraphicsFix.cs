﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GraphicsFix : MonoBehaviour {

    public int DesiredQuality = 1;

	void Start () {
        QualitySettings.SetQualityLevel(DesiredQuality, true);
	}
}
