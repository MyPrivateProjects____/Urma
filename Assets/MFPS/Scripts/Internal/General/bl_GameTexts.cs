﻿public static class bl_GameTexts 
{
    public static string Players = "Гравці";

    public static string PickUpMedicalKit = "Підніміть аптечку";
    public static string PickUpAmmoKit = "Підніміть набої";
    public static string PickUpWeapon = "Натисніть [E/У] щоб підняти <color=#70ABFF>{0}</color>";

    public static string PlayerStart = "<size=14><color=black>{0}</color></size>";
    public static string Killed = "Вбито";
    public static string HeadShot = "Постріл в голову!";
    public static string JoinedInMatch = "Приєднався до завдання";
    public static string LeftOfMatch = "Залишив завдання";
    public static string CommittedSuicide = "Вчинив суїцид";
    public static string KilledEnemy = "Вбито ворога";
    public static string HeatShotBonus = "Бонус постріла в голову";
    public static string JoinIn = "Приєднатися в ";

    public static string ServerMesagge = "[Server]";
    public static string OpenChatStart = "[Server]: Натисніть 'T/Е' або 'Y/Н' щоб відкрити чат";
    public static string NoOneWonName = "Ні один";
    public static string AFKWarning = "Ви поза зоною, Ви програєте за: {0}";

    public static string FinalOneMatch = "Повертайтеся до лоббі";
    public static string FinalRounds = "Наступний раунд";
    public static string FinalWinner = "Раунд виграно!";
    public static string NoRoomsCreated = "КІМНАТ НЕМАЄ, СТВОРІТЬ ХОЧ ОДНУ ЩОБ ВЗЯТИ УЧАСТЬ.";
    //CTF
    public static string CaptureTheFlag = "Захопили прапор";
    public static string ObtainedFlag = "Прапор {0} отримано";

    public static string FireTypeAuto = "АВТО";
    public static string FireTypeSemi = "НАПОЛОВИНУ";
    public static string FireTypeSingle = "ОДИНИЧНИЙ";

    public static string[] AntiStropicOptions = new string[] { "Викл", "Вкл", "Змусити Вкл" };

    public static string RoomsCreated = "<b>КІМНАТ СТВОРЕНО:</b> {0}";
    public static string PlayersOnline = "<b>УЧАСНИКІВ ОНЛАЙН:</b> {0}";
    public static string PlayersPlaying = "<b>УЧАСНИКІВ В БОЯХ:</b> {0}";
    public static string PlayersInLobby = "<b>УЧАСНИКІВ В ЛОББІ:</b> {0}";
}